'''
    Execute with python3 setup.py build_ext --inplace
'''
from setuptools import setup as setup_stools
from setuptools import find_packages
from Cython.Build import cythonize
import numpy

import sys

if sys.version_info < (3, 5):
    print('poisson requires Python 3.5 or above.')
    sys.exit(1)

install_requires = [
                    'scipy',
                    'numpy',
                    'matplotlib',
]

setup_stools(
      name='poisson',
      description='Poisson solver using finite volume',
      author='Poisson authors',
      license='BSD',
      classifiers=[
          'Development Status :: Pre-alpha',
          'Programming Language :: Python :: 3.6',],
      packages=find_packages('.'),
      install_requires=install_requires,
      ext_modules = cythonize(['poisson/discrete/_finite_volume.pyx',
                               'poisson/discrete/_discrete_poisson.pyx',
                               'poisson/discrete/_linear_problem.pyx']),
      include_dirs=[numpy.get_include()]
)
